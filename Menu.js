const { app, Menu } = require("electron");
let win;

function unique(array) {
  return array.filter((v, i, a) => a.indexOf(v) === i);
}

function getHistory() {
  let submenu = [];
  let history = win.webContents.history;
  history = unique(history);
  history = history.filter((url) => url.includes("dominikfladung"));

  for (let i = history.length - 1; i > -1; i--) {
    submenu.push(getLinkSubMenu(history[i], history[i]));
  }
  console.log(submenu);
  return submenu;
}

function getLinkSubMenu(name, link) {
  return {
    label: name,
    click() {
      win.loadURL(link);
    },
  };
}

function createMenu() {
  const template = [
    {
      label: "Main",
      submenu: [],
    },
    {
      label: "Create",
      submenu: [
        getLinkSubMenu("New Snippet", "https://bitbucket.org/snippets/new"),
        getLinkSubMenu("New Repository", "https://bitbucket.org/repo/create"),
      ],
    },
    {
      label: "Read",
      submenu: [
        getLinkSubMenu("Snippets", "https://bitbucket.org/dashboard/snippets"),
        getLinkSubMenu(
          "Repositories",
          "https://bitbucket.org/dashboard/repositories"
        ),
        getLinkSubMenu(
          "Pull Requests",
          "https://bitbucket.org/dashboard/pullrequests"
        ),
      ],
    },
    {
      label: "History",
      submenu: getHistory(),
    },
  ];

  const menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
}

function createDockMenu() {
  if (app.dock) {
    const dockMenu = Menu.buildFromTemplate([
      getLinkSubMenu("Snippets", "https://bitbucket.org/dashboard/snippets"),
      getLinkSubMenu("New Snippet", "https://bitbucket.org/snippets/new"),
    ]);

    app.dock.setMenu(dockMenu);
  }
}

function init(_win) {
  win = _win;
  createMenu();
  createDockMenu();
}

module.exports.initMenu = init;
module.exports.getLinkSubMenu = getLinkSubMenu;
module.exports.createDockMenu = createDockMenu;
module.exports.createMenu = createMenu;
