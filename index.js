const { app, BrowserWindow, globalShortcut } = require("electron");
const { initMenu } = require(__dirname + "/Menu.js");
let oldHistory = [];

app.whenReady().then(appstart);

function appstart() {
  let win = createWindow();
  initMenu(win);
  registerShortcuts(win);

  // TODO FIND EVENT FOR HREF CALLING
  win.webContents.on("open-url", (url) => {
    win.webContents.history.push(url);
  });

  setInterval(() => {
    if (oldHistory != win.webContents.history) {
      oldHistory = win.webContents.history;
      initMenu(win);
    }
  }, 2000);
}

function createWindow() {
  let win = new BrowserWindow({
    title: "Bitbucket-Browser",
    backgroundColor: "#2e2c29",
    id: "main",
  });
  win.loadURL("https://bitbucket.org/dashboard/overview");

  return win;
}

function registerShortcuts(win) {
  globalShortcut.register("CommandOrControl+b+s", () => {
    win.loadURL("https://bitbucket.org/dashboard/snippets");
    win.show();
  });

  globalShortcut.register("CommandOrControl+b+r", () => {
    win.loadURL("https://bitbucket.org/dashboard/repositories");
    win.show();
  });
}

app.on("will-quit", () => {
  globalShortcut.unregisterAll();
});
